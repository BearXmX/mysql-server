/*
 * @Date: 2021-08-13 22:09:28
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-08-14 23:18:29
 * @Description:
 */
const express = require('express');
const mysql = require('mysql');
const app = express();
const consola = require('consola');
const port: number = 3001;
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies */
// 处理端口跨域
app.use('*', function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'PUT,POST,GET,DELETE,OPTIONS'
    );
    res.setHeader('X-Powered-By', ' 3.2.1');
    res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
    next();
});

const sqlServer = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'root',
    database: 'node',
});

sqlServer.connect((err) => {
    if (err) {
        throw err;
    } else {
        console.log('连接数据没报错');
    }
});

app.get('/mysql/getUserInfo', (req, res) => {
    const sql =
        'SELECT jobName,userId FROM user_info INNER JOIN job ON job.jobId = user_info.job';
    const searchUserInfo =
        'SELECT user_info.userId,USER.nickName,job.jobName,user_info.declaration,user_info.age,user_info.isMarriage,nation.nationName FROM user_info LEFT JOIN USER ON USER.userId = user_info.userId LEFT JOIN job ON user_info.job = job.jobId LEFT JOIN nation ON user_info.nation = nation.nationId order by userId';
    sqlServer.query(searchUserInfo, (err, rows, fields) => {
        if (err) consola.info(err);
        else {
            const resData = {
                code: 200,
                message: 'Request is OK',
                result: [...rows],
            };
            res.send(resData);
        }
        //记得关闭连接
    });
});

app.get('/mysql/getAllNation', (req, res) => {
    const searchNation = 'select nationName,nationId value from nation';
    sqlServer.query(searchNation, (err, rows, fields) => {
        if (err) return;
        else {
            const resData = {
                code: 200,
                message: 'Request is OK',
                result: [...rows],
            };
            res.send(resData);
        }
        //记得关闭连接
    });
});

app.get('/mysql/getAllJob', (req, res) => {
    const searchJob = 'select jobName,jobId value from job';
    sqlServer.query(searchJob, (err, rows, fields) => {
        if (err) consola.info(err);
        else {
            const resData = {
                code: 200,
                message: 'Request is OK',
                result: [...rows],
            };
            res.send(resData);
        }
        //记得关闭连接
    });
});

app.post('/mysql/creatUser', (req, res) => {
    const body = JSON.parse(Object.keys(req.body)[0]);
    console.log(body);
    const { nickName, tel, password, age, job, nation, marriage, declaration } =
        body;
    const creatUser = `insert into user ( userId , nickName , tel , password ) values ((select max(userId)+1 from user us), '${nickName}' , ${tel} , ${password} )`;
    const insertCreatUserInfo = `insert into user_info ( userId , age , job , isMarriage , declaration , nation ) values ((select max(userId)+1 from user_info us), ${age} , ${job} ,${
        marriage === 'no' ? 0 : 1
    }, '${declaration}', ${nation})`;
    let flag = false;
    sqlServer.query(creatUser, (err, rows, fields) => {
        if (err) {
            flag = false;
            return;
        } else {
            flag = true;
        }
    });
    sqlServer.query(insertCreatUserInfo, (err, rows, fields) => {
        if (err) {
            flag = false;
            return;
        } else {
            flag = true;
        }
    });
    const resData = {
        code: 200,
        message: 'Request is OK',
    };
    setTimeout(() => {
        if (flag) {
            res.send(resData);
        } else {
            resData.code = 404;
            resData.message = 'Request is failed';
            res.send(resData);
        }
    }, 2000);
});

app.listen(port, () => {
    consola.success(`express打开${port}端口`);
});
